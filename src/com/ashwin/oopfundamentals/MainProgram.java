/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashwin.oopfundamentals;

import com.ashwin.oopfundamentals.checkbook.CheckBook;
import com.ashwin.oopfundamentals.checkbook.CheckBookFactory;
import com.ashwin.oopfundamentals.course.Course;
import com.ashwin.oopfundamentals.course.CourseFactory;
import com.ashwin.oopfundamentals.dog.Dog;
import com.ashwin.oopfundamentals.dog.DogFactory;
import com.ashwin.oopfundamentals.grade.Agrade;
import com.ashwin.oopfundamentals.grade.Bgrade;
import com.ashwin.oopfundamentals.grade.Grade;
import com.ashwin.oopfundamentals.grade.GradeFactory;
import com.ashwin.oopfundamentals.monitor.Monitor;
import com.ashwin.oopfundamentals.monitor.MonitorFactory;

/**
 *
 * @author ashwin
 */
public class MainProgram {

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    //CheckBook related functionalities
    CheckBookFactory checkFactory = new CheckBookFactory();
    //get CheckBook object from CheckBookFactory
    CheckBook checkBook = checkFactory.get("travel");
    checkBook.getExpense();
    checkBook.depositeAmount(5000);
    checkBook.getActiveStatus();
    
    //Courses related functionalities
    CourseFactory courseFactory = new CourseFactory();
    Course course = courseFactory.get("computing");
    course.startCourse();
    course.endCourse();
    course.getCourseDetails();
    
    //dogs related functionalites
    DogFactory dogFactory = new DogFactory();
    Dog dog = dogFactory.get("boxer");
    dog.bark();
    dog.eat();
    dog.run();
    
    //grades related functionalities
    GradeFactory gradeFactory = new GradeFactory();
    Grade grade = gradeFactory.get(70);
    if (grade instanceof Agrade || grade instanceof Bgrade) {
      
    }
    grade.calculatePercentage(70);
    
    //monitors related funcitonalities
    MonitorFactory monitorFactory = new MonitorFactory();
    Monitor monitor = monitorFactory.get("lcd");
    monitor.turnOn();
  }
  
}
