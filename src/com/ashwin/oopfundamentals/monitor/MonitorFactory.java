/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashwin.oopfundamentals.monitor;

/**
 *
 * @author ashwin
 */
public class MonitorFactory {
  
  /**
   * Returns monitor object as per the received monitor type.
   * @param monitor
   * @return mixed
   */
  public Monitor get(String monitor) {
    switch(monitor) {
      case "lcd": 
        return new Lcd();
      case "led":
        return new Led();
      case "crt":
        return new Crt();
      default:
        return null;
    }
  }
  
}
