/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashwin.oopfundamentals.monitor;

/**
 *
 * @author ashwin
 */
public class Led extends Monitor{

  @Override
  public void turnOn() {
    System.out.println("inside led turnOn function");
  }

  @Override
  public void turnOff() {
    System.out.println("inside led turnOff function");
  }

  @Override
  public void display() {
    System.out.println("inside led display function");
  }
  
}
