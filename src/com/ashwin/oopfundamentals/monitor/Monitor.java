/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashwin.oopfundamentals.monitor;

/**
 *
 * @author ashwin
 */
public abstract class Monitor {
  private int height;
  private int width;
  private int color;

  public int getHeight() {
    return height;
  }

  public void setHeight(int height) {
    this.height = height;
  }
  
  public int getWidth() {
    return width;
  }

  public void setWidth(int width) {
    this.width = width;
  }
  
  public int getColor() {
    return color;
  }

  public void setColor(int color) {
    this.color = color;
  }
  
  public abstract void turnOn();
  public abstract void turnOff();
  public abstract void display();
  
}
