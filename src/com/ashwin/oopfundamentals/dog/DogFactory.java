/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashwin.oopfundamentals.dog;

/**
 *
 * @author ashwin
 */
public class DogFactory {
  
  /**
   * Returns dog's object as per the received parameter-dog type.
   * @param type
   * @return mixed
   */
  public Dog get(String type) {
    switch(type) {
      case "boxer":
        return new Boxer();
      case "bulldog":
        return new Bulldog();
      case "labrador":
        return new Labrador();
      case "pog":
        return new Pog();
      default:
        return null;
    }
  }
  
}
