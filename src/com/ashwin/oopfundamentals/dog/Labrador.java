/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashwin.oopfundamentals.dog;

/**
 *
 * @author ashwin
 */
public class Labrador extends Dog{

  @Override
  public void bark() {
    System.out.println("Inside labrador bark function");
  }

  @Override
  public void run() {
    System.out.println("Inside labrador run function");
  }

  @Override
  public void eat() {
    System.out.println("Inside labrador eat function");
  }
  
}
