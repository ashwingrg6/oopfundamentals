/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashwin.oopfundamentals.dog;

/**
 *
 * @author ashwin
 */
public class Bulldog extends Dog{

  @Override
  public void bark() {
    System.out.println("Inside bulldog bark function");
  }

  @Override
  public void run() {
    System.out.println("Inside bulldog run function");
  }

  @Override
  public void eat() {
    System.out.println("Inside bulldog eat function");
  }
  
}
