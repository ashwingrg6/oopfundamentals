/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashwin.oopfundamentals.dog;

/**
 *
 * @author ashwin
 */
public abstract class Dog {
  private String color;
  private int weight;
  private int height;

  public String getColor() {
    return color;
  }
  
  public void setColor(String color) {
    this.color = color;
  }

  public int getWeight() {
    return weight;
  }

  public void setWeight(int weight) {
    this.weight = weight;
  }
  
  public int getHeight() {
    return height;
  }

  public void setHeight(int height) {
    this.height = height;
  }
  
  public abstract void bark();
  public abstract void run();
  public abstract void eat();
  
}
