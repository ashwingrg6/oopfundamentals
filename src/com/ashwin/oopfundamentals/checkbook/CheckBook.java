/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashwin.oopfundamentals.checkbook;

import java.util.Date;

/**
 *
 * @author ashwin
 */
public abstract class CheckBook {
  private int id;
  private Date issuedDate;
  
  public int getId() {
    return id;
  }
  
  public void setId(int id) {
    this.id = id;
  }

  public Date getIssuedDate() {
    return issuedDate;
  }

  public void setIssuedDate(Date issuedDate) {
    this.issuedDate = issuedDate;
  }
  
  public abstract int getExpense();
  public abstract void depositeAmount(int amount);
  public abstract boolean getActiveStatus();
  
}
