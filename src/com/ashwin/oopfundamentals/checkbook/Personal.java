/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashwin.oopfundamentals.checkbook;

/**
 *
 * @author ashwin
 */
public class Personal extends CheckBook {
  private int personalId;

  public Personal() { }

  public Personal(int personalId) {
    this.personalId = personalId;
  }
  
  public int getPersonalId() {
    return personalId;
  }

  public void setPersonalId(int personalId) {
    this.personalId = personalId;
  }
  
  @Override
  public int getExpense() {
    System.out.println("Inside personal checkbook getExpense function");
    return 30000;
  }

  @Override
  public void depositeAmount(int amount) {
    System.out.println("Inside personal checkbook depositeAmount function, amount is "+amount);
  }

  @Override
  public boolean getActiveStatus() {
    System.out.println("Inside personal checkbook getStatus function");
    return false;
  }
  
}
