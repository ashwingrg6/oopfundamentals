/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashwin.oopfundamentals.checkbook;

/**
 *
 * @author ashwin
 */
public class Travel extends CheckBook{
  private int travelCode;
  private String travelCountry;

  public Travel() { }

  public Travel(int travelCode, String travelCountry) {
    this.travelCode = travelCode;
    this.travelCountry = travelCountry;
  }
  
  public int getTravelCode() {
    return travelCode;
  }
  
  public void setTravelCode(int travelCode) {
    this.travelCode = travelCode;
  }

  public String getTravelCountry() {
    return travelCountry;
  }

  public void setTravelCountry(String travelCountry) {
    this.travelCountry = travelCountry;
  }
  
  @Override
  public int getExpense() {
    System.out.println("Inside travel checkbook getExpense function");
    return 20000;
  }

  @Override
  public void depositeAmount(int amount) {
    System.out.println("Inside travel checkbook depositeAmount function, amount is "+amount);
  }

  @Override
  public boolean getActiveStatus() {
    System.out.println("Inside travel checkbook getStatus function");
    return true;
  }
  
}
