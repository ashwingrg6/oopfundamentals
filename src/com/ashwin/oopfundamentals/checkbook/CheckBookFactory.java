/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashwin.oopfundamentals.checkbook;

/**
 *
 * @author ashwin
 */
public class CheckBookFactory {
  
  /**
   * Get checkbook type from parameter and return checkbook object as per the same param.
   * @param type
   * @return mixed
   */
  public CheckBook get (String type) {
    switch (type) {
      case "travel":
        return new Travel();
      case "personal":
        return new Personal();
      case "health":
        return new Health();
      default:
        return null;
    }
  }
}
