/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashwin.oopfundamentals.checkbook;

/**
 *
 * @author ashwin
 */
public class Health extends CheckBook{
  private int healthCode;
  private String healthNote;

  public Health() { }

  public Health(int healthCode) {
    this.healthCode = healthCode;
  }
  
  public int getHealthCode() {
    return healthCode;
  }
  
  public void setHealthCode(int healthCode) {
    this.healthCode = healthCode;
  }

  public String getHealthNote() {
    return healthNote;
  }

  public void setHealthNote(String healthNote) {
    this.healthNote = healthNote;
  }
  
  @Override
  public int getExpense() {
    System.out.println("Inside health checkbook getExpense function");
    return 40000;
  }

  @Override
  public void depositeAmount(int amount) {
    System.out.println("Inside health checkbook depositeAmount function, amount is"+amount);
  }

  @Override
  public boolean getActiveStatus() {
    System.out.println("Inside health checkbook depositeAmount function");
    return true;
  }
  
}
