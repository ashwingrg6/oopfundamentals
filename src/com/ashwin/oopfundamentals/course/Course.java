/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashwin.oopfundamentals.course;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author ashwin
 */
public abstract class Course {
  private String name;
  private Date startDate;
  private Date endDate;
  private int batchYear;

  //setters and getters for class properties
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
  
  public Date getStartDate() {
    return startDate;
  }

  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }
  
  public Date getEndDate() {
    return endDate;
  }

  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }
  
  public int getBatchYear() {
    return batchYear;
  }

  public void setBatchYear(int batchYear) {
    this.batchYear = batchYear;
  }
  
  public abstract void startCourse();
  public abstract void endCourse();
  public abstract ArrayList<String> getCourseDetails();
  
}
