/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashwin.oopfundamentals.course;

import java.util.ArrayList;

/**
 *
 * @author ashwin
 */
public class Multimedia extends Course {
  
  @Override
  public void startCourse() {
    System.out.println("Inside multimedia startCourse function");
  }

  @Override
  public void endCourse() {
    System.out.println("Inside multimedia endCourse function");
  }

  @Override
  public ArrayList<String> getCourseDetails() {
    System.out.println("Inside multimdia getCourseDetails function");
    return null;
  }
  
  private void shootVideo() {
    System.out.println("Inside multimedia shootVideo function");
  }
  
}
