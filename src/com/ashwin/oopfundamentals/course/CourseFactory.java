/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashwin.oopfundamentals.course;

/**
 *
 * @author ashwin
 */
public class CourseFactory {
  
  /**
   * Function to return course object as per the received parameter course type.
   * @param course
   * @return mixed
   */
  public Course get(String course) {
    if(course.equalsIgnoreCase("computing")) {
      return new Computing();
    }
    else if(course.equalsIgnoreCase("networking")) {
      return new Networking();
    }
    else if(course.equalsIgnoreCase("multimedia")) {
      return new Multimedia();
    }
    return null;
  }
  
}
