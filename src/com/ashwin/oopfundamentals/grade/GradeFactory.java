/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ashwin.oopfundamentals.grade;

/**
 *
 * @author ashwin
 */
public class GradeFactory {
  
  /**
   * Returns grade object as per the received marks through parameter.
   * @param marks
   * @return mixed
   */
  public Grade get(int marks) {
    if(marks >= 70) {
      return new Agrade();
    }
    else if(marks >= 60 && marks < 70) {
      return new Bgrade();
    }
    else if(marks >= 50 && marks < 60) {
      return new Cgrade();
    }
    return new Dgrade();
  }
  
}
